    <footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6 text-center text-lg-left">
            <p class="social">
              <a href="https://www.facebook.com/yohan.rucinski/"class="external facebook wow fadeInUp"><span class=>F</span><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/zerowstress" data-wow-delay="0.4s" class="external gplus wow fadeInUp"><span class="">T</span><i class="fa fa-twitter"></i></a>
              <a href="mailto:yohan.rucinski@gmail.com" data-wow-delay="0.6s" class="email wow fadeInUp"><span class=""><img class="fit-picture"
     src="./images/gmail.png"
     alt="gmail"></span><i class="fa fa-envelope"></i></a></p>
          </div>
          <!-- /.6-->
          <div class="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
            <p>© Yohan Rucinski. Tous droits réservés.</p>
          </div>
          <div class="col-12 mt-4">
            <p class="template-bootstrapious">Dernière MAJ 23/12/2020</p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </footer>

 <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/it-worker/2-1/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/it-worker/2-1/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="https://d19m59y37dris4.cloudfront.net/it-worker/2-1/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/it-worker/2-1/vendor/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/it-worker/2-1/vendor/jquery.counterup/jquery.counterup.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js"> </script>
    <script src="https://d19m59y37dris4.cloudfront.net/it-worker/2-1/js/front.js"></script>
    <script src="./app.js"></script>

 <script type="text/javascript">
    $(document).ready(function(){
      $(".section-2").fadeOut(1300, function(){
        $("#content").fadeIn(800);
    });
    })
  </script>
  </body>
</html>