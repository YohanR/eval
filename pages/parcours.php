    <section id="services" class="bg-gradient services-section">
      <div class="container">
        <header class="text-center">
          <h2 data-animate="fadeInDown" class="title">Mon parcours</h2>
        </header>
        <div class="row services text-center">
          <div data-animate="fadeInUp" class="col-lg-4">
            <div class="icon">
              <img class="fit-picture"
                src="./images/certificate.png" width="20%"
                alt="bacalauréat">
            </div>
            <h3 class="heading mb-3 text-400">Bac S</h3>
            <p class="text-left description">Diplomé d'un bac S (sciences de l'ingénieur) durant l'année 2013, j'ai appris les bases de la mécaniques et de la programmation. Ce bac S m'a permis d'être rigoureux et persévérant dans tous les projets qui me sont proposés.</p>
          </div>
          <div data-animate="fadeInUp" class="col-lg-4">
            <div class="icon">
                <img class="fit-picture"
                  src="./images/diploma.png" width="20%"
                  alt="DUT INFO-COM">
            </div>
            <h3 class="heading mb-3 text-400">DUT Info-Com</h3>
            <p class="text-left description">Etant à la base plutôt quelqu'un de solitaire, le DUT communication (année 2014-2016) m'a permis de m'ouvrir au monde et aux personnes qui m'entourent. De plus, j'ai appris à mettre en place des stratégies de communication, à toucher une cible précise et à décrypter les demandes des clients.</p>
          </div>
          <div data-animate="fadeInUp" class="col-lg-4">
            <div class="icon">
                <img class="fit-picture"
                  src="./images/graduated.png" width="20%"
                  alt="DUT INFO-COM">
            </div>
            <h3 class="heading mb-3 text-400">Licence Com digitale</h3>
            <p class="text-left description">Ma licence en communication digitale (2017) m'a permis de réaliser des vidéos, faire des maquettes, des retouches photos, créer des logos etc... Des tâches qui sont normalement réalisées en ammont pour ensuite être développées ou utilisées par le pôle informatique.</p>
          </div>
        </div>
        <hr data-animate="fadeInUp">
        <div data-animate="fadeInUp" class="text-center">
          <p class="lead">Tu veux me connaître plus ou juste discuter ?</p>
          <p><a href="#contact" class="btn btn-outline-light link-scroll">Contacte moi</a></p>
        </div>
      </div>
    </section>
   