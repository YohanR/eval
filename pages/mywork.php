<section id="references">
      <div class="container">
        <div class="col-sm-12">
          <div class="mb-5 text-center">
            <h2 data-animate="fadeInUp" class="title">Mes projets</h2>
            <p data-animate="fadeInUp" class="lead">J'ai travaillé sur quelques projets le long de ma formation en voici quelques uns.</p>
          </div>
          <ul id="filter" data-animate="fadeInUp">
            <li class="active"><a href="#" data-filter="all">All</a></li>
            <li><a href="#" data-filter="Javascript">Javascript</a></li>
            <li><a href="#" data-filter="PHP">PHP</a></li>
            <li><a href="#" data-filter="HTML">HTML/CSS</a></li>
            <li><a href="#" data-filter="SQL">SQL</a></li>
          </ul>
          <div id="detail">
            <div class="row">
              <div class="col-lg-10 mx-auto"><span class="close">×</span>
                <div id="detail-slider" class="owl-carousel owl-theme"></div>
                <div class="text-center">
                  <h1 id="detail-title" class="title"></h1>
                </div>
                <div id="detail-content"></div>
              </div>
            </div>
          </div>
          <!-- Reference detail-->
          <div id="references-masonry" data-animate="fadeInUp">
            <div class="row">
                  <div data-category="Javascript" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/Pen_and_paper_cover.jpg" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">RPG</h3>
                            <p>Un petit RPG codé en Javascript</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg,img/main-slider2.jpg,img/main-slider3.jpg" class="sr-only reference-description">
                        <p>Nous avions comme projet de réaliser un RPG. On devait pouvoir mettre les stats que l'on veut à un héros et le faire affronter plusieurs ennemis. Ces ennemis ont bien sur des compétences spéciales.</p>
                        <p class="buttons text-center"><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-globe"></i> Voir le projet</a><a href="https://gitlab.com/YohanR/rpg" class="btn btn-outline-primary"><i class="fa fa-download"></i> Télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="PHP" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/overwatch-cover.webp" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Mini site php</h3>
                            <p>Site en Php</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider3.jpg,img/main-slider1.jpg" class="sr-only reference-description">
                        <p>Nous devions créer un site sur le sujet que nous souhaitions (overwatch pour ma part) et réussir à appeler nos pages avec du php.</p>
                        <p class="buttons text-center"><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-globe"></i> Voir le projet</a><a href="https://gitlab.com/labege_carbonne/mini-site-php" class="btn btn-outline-primary"><i class="fa fa-download"></i> Télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="HTML" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/boot.jpeg" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Projet boostrap</h3>
                            <p>Une simple réalisation Bootstrap</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg,img/main-slider2.jpg,img/main-slider3.jpg" class="sr-only reference-description">
                        <p>Afin de nous améliorer sur le framework Bootsrap, nous avons dû reproduire à la perfection une maquette, voici le resultat.</p>
                        <p class="buttons text-center"><a href="http://www.exobootstrap.surge.sh" class="btn btn-outline-primary"><i class="fa fa-globe"></i> Voir le projet</a><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-download"></i> télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="Javascript" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/grandmaster.png" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Utiliser une API</h3>
                            <p>Utilisation de l'API Blizzard</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg,img/main-slider2.jpg,img/main-slider3.jpg" class="sr-only reference-description">
                        <p>Nous devions par le biais de l'API Blizzard, récupérer les infos du joueur liées à son Battletag.
                        Bien évidemment, nous avons dû mettre certaines conditions sur les formulaires.</p>
                        <p class="buttons text-center"><a href="https://overwatch-mmy.surge.sh/" class="btn btn-outline-primary"><i class="fa fa-globe"></i> Voir le projet</a><a href="https://gitlab.com/marvin31390/overwatch" class="btn btn-outline-primary"><i class="fa fa-download"></i> Télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="HTML" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/bulma.webp" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Projet Bulma</h3>
                            <p>Une simple réalisation Bulma</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg,img/main-slider2.jpg,img/main-slider3.jpg" class="sr-only reference-description">
                        <p>Afin de nous améliorer sur le framework Bulma, nous avions dû reproduire à la perfection une maquette, voici le resultat</p>
                        <p class="buttons text-center"><a href="les_cucurbitaces2.surge.sh" class="btn btn-outline-primary"><i class="fa fa-globe"></i> voir le projet</a><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-download"></i> télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="SQL" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/mysql.png" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Php Et SQL</h3>
                            <p>Utilisattion d'une bdd en php</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg,img/main-slider2.jpg,img/main-slider3.jpg" class="sr-only reference-description">
                        <p>Nous avons créé une base de données avec MYSQL pour ensuite procéder à plusieurs appels en php. </p>
                        <p class="buttons text-center"><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-globe"></i> voir le projet</a><a href="https://gitlab.com/YohanR/cinema-3" class="btn btn-outline-primary"><i class="fa fa-download"></i> télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="HTML" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/refonte-site-internet-1.jpg" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Refonte d'un site</h3>
                            <p>Refonte d'un site avec bootstrap (en un jour)</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg" class="sr-only reference-description">
                        <p>Refonte du site mi-aime-a-ou.com avec bootstrap afin de le remettre au goût du jour.</p>
                        <p class="buttons text-center"><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-globe"></i> voir le projet</a><a href="https://gitlab.com/YohanR/mi-aime" class="btn btn-outline-primary"><i class="fa fa-download"></i> télécharger le projet</a></p>
                      </div>
                    </div>
                  </div>
                  <div data-category="Javascript" class="reference-item col-lg-3 col-md-6">
                    <div class="reference"><a href="#"><img src="./images/json-logo.png" alt="" class="img-fluid">
                        <div class="overlay">
                          <div class="inner">
                            <h3 class="h4 reference-title">Animalerie</h3>
                            <p>Récupérer des infos d'un fichier .Json</p>
                          </div>
                        </div></a>
                      <div data-images="img/main-slider1.jpg,img/main-slider2.jpg,img/main-slider3.jpg" class="sr-only reference-description">
                        <p>Nous devions récupérer des infos d'un fichier Json pour les afficher dans la console puis à l'écran.</p>
                        <p class="buttons text-center"><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-globe"></i> voir le projet</a><a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-download"></i> télécharger le prjet</a></p>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </section>