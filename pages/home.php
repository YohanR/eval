
    <!-- Reference item-->
    <!-- navbar-->

    </header>
     <section id="chargement" class=" section-2">
      <span class="loader loader-double noirnoir"></span>
       <span class="noirnoir">Chargement...</span>
      </section>
    <!-- Intro Image-->
    <section id="content">
    <section id="intro" style="background-color: #4fbfa8; background-size: cover;" class="intro-section pb-2">
      <div class="container text-center">
        <div data-animate="fadeInDown" class="logo"><img src="./images/logow.png" alt="logo" width="280"></div>
        <h1 data-animate="fadeInDown" class="text-shadow mb-5 noir"><strong>Yohan Rucinski</strong></h1>
        <p data-animate="slideInUp" class="h3 text-shadow text-400 noir"><strong>Actuellement en formation de developpeur web et web mobile à Simplon, je vous laisse découvrir mon portfolio pour en apprendre plus sur moi.</strong></p>
      </div>
    </section>
    <!-- About-->
    <section id="about" class="about-section">
      <div class="container">
        <header class="text-center">
          <h2 data-animate="fadeInDown" class="title">À propos</h2>
        </header>
        <div class="row">
          <div data-animate="fadeInUp" class="col-lg-6">
            <p>Je m'appelle Yohan Rucinski, nous sommes actuellement en <strong>2020</strong> (sale année) et je viens de faire <strong>26</strong> ans. Je suis quelqu'un de <strong>passionné</strong> par <strong>l'informatique</strong> depuis tout petit. Dès mon plus jeune âge, je commençais déja à "bidouiller" des ordinateurs.</p>
            <p>J'aime beaucoup tout ce qui est <strong>travail en équipe</strong> car nos collègues sont souvent source de savoirs.
              Je suis quelqu'un de <strong>sérieux</strong> et j'aime me donner à fond quand le projet me tient a coeur.
              Durant cette année 2020, je me suis donc lancé le défi de devenir <strong>développeur Web et Web Mobile</strong> en suivant la formation <strong>Simplon.</strong></p>
            <p>Durant cette formation, j'apprends à développer un <strong>site web</strong> par le biais de divers langages informatiques comme : le <strong>HTML</strong>, le <strong>CSS</strong>, le <strong>PHP</strong>, le <strong>Javascript</strong> (cette liste n'est pas exhaustive) ... Pour plus d'informations, contactez-moi.</p>
          </div>
          <div data-animate="fadeInUp" class="col-lg-6">
            <div class="skill-item">
              <div class="progress-title">PHP</div>
              <div class="progress">
                <div role="progressbar" style="width: 60%" aria-valuenow="0" aria-valuemin="60" aria-valuemax="100" class="progress-bar progress-bar-skill1"></div>
              </div>
            </div>
            <div class="skill-item">
              <div class="progress-title">Javascript</div>
              <div class="progress">
                <div role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="60" aria-valuemax="100" class="progress-bar progress-bar-skill2"></div>
              </div>
            </div>
            <div class="skill-item">
              <div class="progress-title">HTML</div>
              <div class="progress">
                <div role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-skill3"></div>
              </div>
            </div>
            <div class="skill-item">
              <div class="progress-title">SEO</div>
              <div class="progress">
                <div role="progressbar" style="width: 40%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-skill4"></div>
              </div>
            </div>
            <div class="skill-item">
              <div class="progress-title">MYSQL</div>
              <div class="progress">
                <div role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-skill5"></div>
              </div>
            </div>
          </div>
          <div data-animate="fadeInUp" class="col-sm-12 text-center col-mt-5"><img src="./images/profil.jpg" alt="This is me - IT worker" class="image rounded-circle img-fluid"></div>
        </div>
      </div>
    </section>
  </section>
 
   


