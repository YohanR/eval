
    


    <section id="contact" data-animate="bounceIn" class="contact-section contact">
      <div class="container">
        <header class="text-center">
          <h2 class="title">Contact</h2>
        </header>
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <form id="contact-form" method="post" action="">
              <div class="messages"></div>
              <div class="controls">
                <div class="row">
                    <label class="col-md-6">
                      Prénom
                    <input id="prenom" type="text" name="name" placeholder="Prénom *"  class="form-control">
                  </label>
                    <label class="col-md-6">
                      Nom
                    <input id="nom" type="text" name="surname" placeholder="Nom *" required="required" class="form-control">
                  </label>
                    <label class="col-md-6">
                      Email
                    <input id="email" type="text" name="email" placeholder="Email *" required="required" class="form-control">
                  </label>
                    <label class="col-md-6">
                      Téléphone
                    <input id="tel" type="text" name="phone"  placeholder="Télephone *" class="form-control">
                  </label>
                    <label class="col-md-12">
                      Message
                    <textarea id="msg" name="message" placeholder="Message*" rows="4" required="required" class="form-control"></textarea>
                  </label>
                  <div class="col-md-12 text-center">
                    <button type="button" onclick="verif(), displayClean()" class="btn btn-outline-primary"><span class="noir">Envoyer le message</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
   