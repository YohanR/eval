function sendMail(params) {
    let tempParams = {
        from_name: document.getElementById("nom").value,
        e_mail: document.getElementById("email").value,
        message: document.getElementById("msg").value,
    }
    emailjs.send('service_clrvwdp', 'template_wbk3t6k', tempParams )
    .then(function(res){
        console.log('success', res.status)
        window.alert("votre message a bien été envoyé");
    })
}

function displayClean() {

    //Clean input nom
    let fromName = document.getElementById('nom');
    fromName.value = "";
    //Clean input email
    let eMail = document.getElementById('email');
    eMail.value = "";
    //Clean input message
    let msg = document.getElementById('msg');
    msg.value = "";
    let prenom = document.getElementById('prenom');
    prenom.value = "";
    let tel = document.getElementById('tel');
    tel.value = "";
};
 
function verif(){
    if(document.getElementById("nom").value.length<=0 && document.getElementById("email").value.length<=0 && document.getElementById("msg").value<=0){
        window.alert("remplissez au moins le Nom, le mail et le message")
    }else{
        sendMail();
    }
}